package com.trytara.subwaytalent;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * 
 * @author JRDomingo
 * Dec 10, 2016
 */
@SpringBootApplication
public class SubwaytalentApplication {
	public static final String DATE_FORMAT = "yyyy-MM-dd\'T\'HH:mm:ssZZZZZ";
	public static void main(String[] args) {
		SpringApplication.run(SubwaytalentApplication.class, args);
	}
}
