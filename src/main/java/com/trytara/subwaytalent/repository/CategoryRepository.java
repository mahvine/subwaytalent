/**
 * 
 */
package com.trytara.subwaytalent.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import com.trytara.subwaytalent.model.Category;

/**
 * @author JRDomingo
 * @since Dec 22, 2016
 *
 */
public interface CategoryRepository extends JpaRepository<Category, Long>, JpaSpecificationExecutor<Category>{
	
}
