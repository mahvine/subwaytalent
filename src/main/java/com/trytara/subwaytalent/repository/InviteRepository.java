/**
 * 
 */
package com.trytara.subwaytalent.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import com.trytara.subwaytalent.model.Invite;;

/**
 * @author JRDomingo
 * @since Dec 14, 2016
 *
 */
public interface InviteRepository extends JpaRepository<Invite, Long>, JpaSpecificationExecutor<Invite>{
	
	
}
