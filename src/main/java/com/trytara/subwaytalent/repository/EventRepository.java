/**
 * 
 */
package com.trytara.subwaytalent.repository;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import com.trytara.subwaytalent.model.Event;
import com.trytara.subwaytalent.model.Event.EventStatus;
import com.trytara.subwaytalent.model.User;;

/**
 * @author MMMagallanes
 * @since Dec 11, 2016
 *
 */
public interface EventRepository extends JpaRepository<Event, Long>, JpaSpecificationExecutor<Event>{
	List<Event> findByStatus(EventStatus status);
	
	List<Event> findByCategory(String category);
	
	List<Event> findByLocationContainsIgnoreCase(String location);
	
	Page<Event> findByCreatedBy(User user, Pageable pageable);
	
}
