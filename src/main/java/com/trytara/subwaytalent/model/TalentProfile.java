package com.trytara.subwaytalent.model;

import java.util.Set;

import javax.persistence.CollectionTable;
import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

/**
 * describes a {@link User}'s talents
 * @author JRDomingo
 * 
 */
@Entity
@Table(name="talent_profile")
public class TalentProfile {
	
	/**
	 * Youtube URL (videos)
	 * Soundcloud URL (music)
	 * Sample Image URLs(photography)
	 */
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	public Long id;
	
	@OneToOne
	@JoinColumn(name="user_id", referencedColumnName="id")
	public User user;
	
	@Column(name="soundcloud_username", unique=true)
	public String soundcloudUsername;

	@Column(name="youtube_username", unique=true)
	public String youtubeUsername;
	
	@ElementCollection(fetch = FetchType.EAGER)
	@CollectionTable(name = "talent_profile_gallery", joinColumns = @JoinColumn(name = "talent_profile_id", referencedColumnName="id"))
	@Column(name = "image_urls", length = 100)
	public Set<String> galleryImageUrls;
	
	public String bio;

	public String location;

	@ElementCollection(fetch = FetchType.EAGER)
	@CollectionTable(name = "talent_genre", joinColumns = @JoinColumn(name = "talent_id", referencedColumnName="id"))
	@Column(name = "genre", length = 100)
	public Set<String> genres;
	
	@ElementCollection(fetch = FetchType.EAGER)
	@CollectionTable(name = "talent_skill", joinColumns = @JoinColumn(name = "talent_id", referencedColumnName="id"))
	@Column(name = "skill", length = 100)
	public Set<String> skills;
	
}
