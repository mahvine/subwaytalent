package com.trytara.subwaytalent.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.trytara.subwaytalent.model.Invite.InvitePK;

/**
 * An invitation for an {@link Event} sent by a {@link User} to another {@link User} 
 * @author JRDomingo
 * @since Dec 10, 2016
 */
@Entity
@Table(name="invite")
@IdClass(InvitePK.class)
public class Invite {

	@Id
	@ManyToOne
	@JoinColumn(name="user_id",referencedColumnName="id")
	public User user;

	@Id
	@ManyToOne
	@JoinColumn(name="event_id",referencedColumnName="id")
	public Event event;
	
	@Column(name="date_created")
	public Date dateCreated;
	
	public String status;
	
	public enum Status{
		CREATED, ACCEPTED, CANCELLED, DECLINED
	}


	@SuppressWarnings("serial")
	public static class InvitePK implements Serializable{
		private Long user;
		private Long event;
		public Long getUser() {
			return user;
		}
		public void setUser(Long user) {
			this.user = user;
		}
		public Long getEvent() {
			return event;
		}
		public void setEvent(Long event) {
			this.event = event;
		}
		@Override
		public int hashCode() {
			final int prime = 31;
			int result = 1;
			result = prime * result + ((event == null) ? 0 : event.hashCode());
			result = prime * result + ((user == null) ? 0 : user.hashCode());
			return result;
		}
		@Override
		public boolean equals(Object obj) {
			if (this == obj)
				return true;
			if (obj == null)
				return false;
			if (getClass() != obj.getClass())
				return false;
			InvitePK other = (InvitePK) obj;
			if (event == null) {
				if (other.event != null)
					return false;
			} else if (!event.equals(other.event))
				return false;
			if (user == null) {
				if (other.user != null)
					return false;
			} else if (!user.equals(other.user))
				return false;
			return true;
		}
		
	}
}
