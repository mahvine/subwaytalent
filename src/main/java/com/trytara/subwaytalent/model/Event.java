package com.trytara.subwaytalent.model;


import java.util.Date;
import java.util.Set;

import javax.persistence.CollectionTable;
import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;


/**
 * Represents a 'Gig' that is created by a {@link User}
 * @author JRDomingo
 * 
 */
@Entity
public class Event {

	@Id
	@GeneratedValue(strategy= GenerationType.AUTO)
	@Column(name="id")
	public Long id;
	
	@Column(name="title")
	public String title;
	
	@Column(name="description")
	public String description;
	
	public String type;
	
	@Column(name="category")
	public String category;
	

	@ElementCollection(fetch = FetchType.EAGER)
	@CollectionTable(name = "event_genre", joinColumns = @JoinColumn(name = "event_id", referencedColumnName="id"))
	@Column(name = "preferred_genre", length = 100)
	public Set<String> preferredGenres;
	
	
	@ElementCollection(fetch = FetchType.EAGER)
	@CollectionTable(name = "event_skill", joinColumns = @JoinColumn(name = "event_id", referencedColumnName="id"))
	@Column(name = "preferred_skill", length = 100)
	public Set<String> preferredSkills;
	
	

	@Column(name="start_date")
	public Date dateStart;

	@Column(name="end_date")
	public Date dateEnd;
	
	@JsonIgnore
	@ManyToOne(targetEntity = User.class)
	@JoinColumn(name="created_by",referencedColumnName="id")
	public User createdBy;
	
	@JsonFormat(pattern="yyyy-MM-dd’T’HH:mm:ssZZ")
	@Column(name="creation_date")
	public Date dateCreated;
	
	@Column(name="location")
	public String location;
	
	@Column(name="photo_filename")
	public String picture;

	public String status;
	
	public enum EventStatus{
		CREATED, FINISHED, CANCELLED
	}

	@Override
	public String toString() {
		return "{" + (id != null ? "id : " + id + ", " : "") + (title != null ? "title : " + title + ", " : "")
				+ (description != null ? "description : " + description + ", " : "")
				+ (type != null ? "type : " + type + ", " : "")
				+ (category != null ? "category : " + category + ", " : "")
				+ (preferredGenres != null ? "preferredGenre : " + preferredGenres + ", " : "")
				+ (preferredSkills != null ? "preferredSkills : " + preferredSkills + ", " : "")
				+ (dateStart != null ? "dateStart : " + dateStart + ", " : "")
				+ (dateEnd != null ? "dateEnd : " + dateEnd + ", " : "")
				+ (createdBy != null ? "createdBy : " + createdBy + ", " : "")
				+ (dateCreated != null ? "dateCreated : " + dateCreated + ", " : "")
				+ (location != null ? "location : " + location + ", " : "")
				+ (picture != null ? "picture : " + picture + ", " : "") + (status != null ? "status : " + status : "")
				+ "}";
	}
	
	
}
