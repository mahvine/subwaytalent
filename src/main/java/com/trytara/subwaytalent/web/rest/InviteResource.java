package com.trytara.subwaytalent.web.rest;

import java.net.URISyntaxException;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.trytara.subwaytalent.model.Invite;
import com.trytara.subwaytalent.repository.InviteRepository;
import com.trytara.subwaytalent.web.rest.filter.InviteFilter;
import com.trytara.subwaytalent.web.rest.util.PaginationUtil;

@RestController
@RequestMapping("/api")
public class InviteResource {
	
	@Autowired
	InviteRepository inviteRepository;

	@RequestMapping(value="/invites", method=RequestMethod.POST)
	public void save(@RequestBody Invite invite){
		inviteRepository.save(invite);
	}

	@RequestMapping(value="/invites", method=RequestMethod.GET)
	public ResponseEntity<List<Invite>> list(@RequestParam(value="page", defaultValue="1") int page, 
			@RequestParam(value="per_page", defaultValue="10") int size,
			@RequestParam(value="sort", defaultValue="id") String sort,
			@RequestParam(value="direction", defaultValue="DESC") Direction direction,
			HttpServletRequest request) throws URISyntaxException{
		Pageable pageRequest = PaginationUtil.generatePageRequest(page, size, new Sort(direction,sort));
		Page<Invite> pageResult = inviteRepository.findAll(new InviteFilter(request), pageRequest);
		HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(pageResult, "/api/invites", page, size);
		return new ResponseEntity<List<Invite>>(pageResult.getContent(), headers, HttpStatus.OK);
	}
	
	
	@RequestMapping(value="/invites/{id}", method=RequestMethod.GET)
	public ResponseEntity<Invite> get(@PathVariable("id") Long id) throws URISyntaxException{
		Invite invite = inviteRepository.findOne(id);
		return new ResponseEntity<Invite>(invite, HttpStatus.OK);
	}

}
