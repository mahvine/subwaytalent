package com.trytara.subwaytalent.web.rest.dto;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.trytara.subwaytalent.model.TalentProfile;
import com.trytara.subwaytalent.model.User;

/**
 * 
 * @author JRDomingo
 *
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
public class TalentDTO {
	
    public Long id;
    public String email;
    public String name;
    public String pictureUrl;
	public String soundcloudUsername;

	public Float cummulativeRating;

	public Float hourlyRate;
    
	public String youtubeUsername;
	
	public Set<String> galleryImageUrls;
	
	public String bio;
	
    public String location;

	public Set<String> genres;
	
	public Set<String> skills;

    public TalentDTO() {
    }

    public TalentDTO(User user) {
        if (user.profile != null) {
            this.name = user.profile.name;
            this.pictureUrl = user.profile.pictureUrl;
        }
        this.id = user.id;
        this.email = user.email;
    }

    public TalentDTO(TalentProfile talentProfile) {
    	User user =talentProfile.user;
        if (user.profile != null) {
            this.name = user.profile.name;
            this.pictureUrl = user.profile.pictureUrl;
        }
        this.id = user.id;
        this.email = user.email;
        this.bio = talentProfile.bio;
        this.location = talentProfile.location;
        this.cummulativeRating = user.cummulativeRating;
        this.hourlyRate = user.hourlyRate;
        this.galleryImageUrls = talentProfile.galleryImageUrls;
        this.genres = talentProfile.genres;
        this.skills = talentProfile.skills;
        this.soundcloudUsername = talentProfile.soundcloudUsername;
        this.youtubeUsername = talentProfile.youtubeUsername;
    }
    
    public static List<TalentDTO> toList(List<TalentProfile> talentProfiles){
    	List<TalentDTO> talentDTOs = new ArrayList<TalentDTO>();
    	for(TalentProfile talentProfile: talentProfiles){
    		talentDTOs.add(new TalentDTO(talentProfile));
    	}
    	return talentDTOs;
    }
}
