/**
 * 
 */
package com.trytara.subwaytalent.web.rest.dto.account;

import java.util.Date;

/**
 * @author JRDomingo
 * 
 */
public class FacebookLoginDTO{

	public String facebookId;
	
    public String name;
    
    public String pictureUrl;
    
    public Date birthDate;
	
}
