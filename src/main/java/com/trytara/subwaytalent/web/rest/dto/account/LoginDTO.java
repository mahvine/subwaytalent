/**
 * 
 */
package com.trytara.subwaytalent.web.rest.dto.account;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * 
 * @author JRDomingo
 *
 */
@ApiModel()
public class LoginDTO {

	@NotNull
	@Size(min=1,message="Invalid email")
	@ApiModelProperty(value = "Email Address ", required = true,position=0)
	public String email;

	@NotNull
	@Size(min=6,message="Invalid password must be at least 6 characters")
	@ApiModelProperty(value = "Must be at least 6 characters ", required = true, position=1)
	public String password;
	
}
