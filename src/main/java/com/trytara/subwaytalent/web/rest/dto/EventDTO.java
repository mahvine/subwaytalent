package com.trytara.subwaytalent.web.rest.dto;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Set;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.trytara.subwaytalent.SubwaytalentApplication;
import com.trytara.subwaytalent.model.Event;

public class EventDTO {

	public Long id;
	
	public String title;
	
	public String description;
	
	public String type;
	
	public String category;
	
	public Set<String> preferredGenres;
	
	public Set<String> preferredSkills;

	@JsonFormat(pattern=SubwaytalentApplication.DATE_FORMAT)
	public Date dateStart;

	@JsonFormat(pattern=SubwaytalentApplication.DATE_FORMAT)
	public Date dateEnd;
	
	@JsonFormat(pattern=SubwaytalentApplication.DATE_FORMAT)
	public Date dateCreated;
	
	public String location;
	
	public String picture;

	public String status;
	
	public EventDTO(){}
	
	public EventDTO(Event event){
		this.id = event.id;
		this.category = event.category;
		this.dateCreated = event.dateCreated;
		this.dateEnd = event.dateEnd;
		this.dateStart = event.dateStart;
		this.description = event.description;
		this.location = event.location;
		this.title = event.title;
		this.picture = event.picture;
		this.status = event.status;
		this.type = event.type;
		if(event.preferredGenres!=null){
			this.preferredGenres = event.preferredGenres;
		}
		if(event.preferredSkills!=null){
			this.preferredSkills = event.preferredSkills;
		}
	}
	
	
	public Event toEvent(){
		Event event = new Event();
		event.id = this.id;
		event.category = this.category;
		event.dateCreated = this.dateCreated;
		event.dateEnd = this.dateEnd;
		event.dateStart = this.dateStart;
		event.description = this.description;
		event.location = this.location;
		event.title = this.title;
		event.picture = this.picture;
		event.status = this.status;
		event.type = this.type;
		event.preferredGenres = this.preferredGenres;
		event.preferredSkills = this.preferredSkills;
		return event;
	}
	
	public static List<EventDTO> toDTOs(List<Event> events){
		List<EventDTO> dtos = new ArrayList<EventDTO>();
		for(Event event:events){
			dtos.add(new EventDTO(event));
		}
		return dtos;
	}

	@Override
	public String toString() {
		return "{" + (id != null ? "id : " + id + ", " : "") + (title != null ? "title : " + title + ", " : "")
				+ (description != null ? "description : " + description + ", " : "")
				+ (type != null ? "type : " + type + ", " : "")
				+ (category != null ? "category : " + category + ", " : "")
				+ (preferredGenres != null ? "preferredGenre : " + preferredGenres + ", " : "")
				+ (preferredSkills != null ? "preferredSkills : " + preferredSkills + ", " : "")
				+ (dateStart != null ? "dateStart : " + dateStart + ", " : "")
				+ (dateEnd != null ? "dateEnd : " + dateEnd + ", " : "")
				+ (dateCreated != null ? "dateCreated : " + dateCreated + ", " : "")
				+ (location != null ? "location : " + location + ", " : "")
				+ (picture != null ? "picture : " + picture + ", " : "") + (status != null ? "status : " + status : "")
				+ "}";
	}
	
	
}
