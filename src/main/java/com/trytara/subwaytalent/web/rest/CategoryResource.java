package com.trytara.subwaytalent.web.rest;

import java.net.URISyntaxException;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.trytara.subwaytalent.model.Category;
import com.trytara.subwaytalent.repository.CategoryRepository;
import com.trytara.subwaytalent.web.rest.filter.CategoryFilter;
import com.trytara.subwaytalent.web.rest.util.PaginationUtil;

@RestController
@RequestMapping("/api")
public class CategoryResource {
	
	@Autowired
	CategoryRepository categoryRepository;

	@RequestMapping(value="/categories", method=RequestMethod.POST)
	public void save(@RequestBody Category category){
		categoryRepository.save(category);
	}
	

	@RequestMapping(value="/categories", method=RequestMethod.GET)
	public ResponseEntity<List<Category>> list(@RequestParam(value="page", defaultValue="1") int page, 
			@RequestParam(value="per_page", defaultValue="10") int size,
			@RequestParam(value="sort", defaultValue="id") String sort,
			@RequestParam(value="direction", defaultValue="DESC") Direction direction,
			HttpServletRequest request) throws URISyntaxException{
		Pageable pageRequest = PaginationUtil.generatePageRequest(page, size, new Sort(direction,sort));
		Page<Category> pageResult = categoryRepository.findAll(new CategoryFilter(request), pageRequest);
		HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(pageResult, "/api/categories", page, size);
		return new ResponseEntity<List<Category>>(pageResult.getContent(), headers, HttpStatus.OK);
	}

	@RequestMapping(value="/categories/{id}", method=RequestMethod.GET)
	public ResponseEntity<Category> get(@PathVariable("id") Long id) throws URISyntaxException{
		Category category = categoryRepository.findOne(id);
		return new ResponseEntity<Category>(category, HttpStatus.OK);
	}

	

}
