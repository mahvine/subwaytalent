package com.trytara.subwaytalent.web.rest;

import java.net.URISyntaxException;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.trytara.subwaytalent.model.TalentProfile;
import com.trytara.subwaytalent.model.User;
import com.trytara.subwaytalent.repository.TalentProfileRepository;
import com.trytara.subwaytalent.repository.UserRepository;
import com.trytara.subwaytalent.service.exception.GenericUserException;
import com.trytara.subwaytalent.service.exception.GenericUserException.InvalidSessionToken;
import com.trytara.subwaytalent.web.rest.dto.TalentDTO;
import com.trytara.subwaytalent.web.rest.filter.TalentProfileFilter;
import com.trytara.subwaytalent.web.rest.util.PaginationUtil;

@RestController
@RequestMapping("/api")
public class TalentResource {
	
	private static final Logger logger = LoggerFactory.getLogger(TalentResource.class);

	@Autowired
	TalentProfileRepository talentProfileRepository;

	@Autowired
	UserRepository userRepository;
	
	@Autowired
	UserResource userResource;

	@RequestMapping(value="/talents/me", method=RequestMethod.PUT)
	public void updateProfile(@RequestBody TalentDTO request, @RequestHeader("X-session") String sessionToken){
		User user = userResource.getUser(sessionToken);
		if(user!=null){
			logger.info("User found for sessionToken:{}",sessionToken);
			TalentProfile talentProfile = talentProfileRepository.findOneByUser(user);
			if(talentProfile == null){
				logger.info("TalentProfile is not found for user:{}",user);
				talentProfile = new TalentProfile();
				talentProfile.user = user;
			}
			
			talentProfile.bio = request.bio;
			talentProfile.galleryImageUrls = request.galleryImageUrls;
			talentProfile.genres = request.genres;
			talentProfile.skills = request.skills;
			talentProfile.soundcloudUsername = request.soundcloudUsername;
			talentProfile.youtubeUsername = request.youtubeUsername;

			talentProfileRepository.save(talentProfile);
			logger.info("talentProfile save:{}",talentProfile);
			
		}else{
			logger.info("InvalidSessionToken for sessionToken:{}",sessionToken);
			throw new InvalidSessionToken();
		}
	}


	@RequestMapping(value="/talent", method=RequestMethod.GET)
	public ResponseEntity<List<TalentDTO>> list(@RequestParam(value="page", defaultValue="1") int page, 
			@RequestParam(value="per_page", defaultValue="10") int size,
			@RequestParam(value="sort", defaultValue="id") String sort,
			@RequestParam(value="search", required=false) String search,
			@RequestParam(value="direction", defaultValue="DESC") Direction direction,
			HttpServletRequest request) throws URISyntaxException{
		Pageable pageRequest = PaginationUtil.generatePageRequest(page, size, new Sort(direction,sort));
		Page<TalentProfile> pageResult = talentProfileRepository.findAll(new TalentProfileFilter(request), pageRequest);
		HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(pageResult, "/api/talentProfiles", page, size);
		return new ResponseEntity<List<TalentDTO>>(TalentDTO.toList(pageResult.getContent()), headers, HttpStatus.OK);
	}


	@RequestMapping(value="/talent/{id}", method=RequestMethod.GET)
	public TalentDTO getById(@PathVariable("id") Long id) throws URISyntaxException{
		User user = userRepository.findOne(id);
		if(user!=null){
			TalentProfile talentProfile = talentProfileRepository.findOneByUser(user);
			if(talentProfile != null){
				return new TalentDTO(talentProfile);
			}
		}
		throw new GenericUserException("Invalid Talent id");
	}
	

}
