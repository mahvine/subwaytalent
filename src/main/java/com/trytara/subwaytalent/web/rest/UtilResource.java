package com.trytara.subwaytalent.web.rest;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.trytara.subwaytalent.model.User;
import com.trytara.subwaytalent.repository.UserRepository;


@RestController
@RequestMapping("/util")
public class UtilResource {
	
	@Autowired
	UserRepository userRepository;

	@RequestMapping(value="/api/users", method=RequestMethod.GET)
	public List<User> listAllUsers(){
		return userRepository.findAll();
	}

	@RequestMapping(value="/api/delete", method=RequestMethod.DELETE)
	public void delete(@RequestParam("email") String email){
		User user = userRepository.findByEmailIgnoreCase(email);
		if(user!=null){
			userRepository.delete(user);
		}
	}
	

	@RequestMapping(value="/api/activate", method=RequestMethod.GET)
	public void activate(@RequestParam("email") String email){
		User user = userRepository.findByEmailIgnoreCase(email);
		if(user!=null){
			user.status = User.Status.VERIFIED;
			userRepository.save(user);
		}
	}
	

}
