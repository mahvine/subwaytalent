package com.trytara.subwaytalent.web.rest;

import java.net.URISyntaxException;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.trytara.subwaytalent.model.Event;
import com.trytara.subwaytalent.model.Event.EventStatus;
import com.trytara.subwaytalent.model.User;
import com.trytara.subwaytalent.repository.EventRepository;
import com.trytara.subwaytalent.web.rest.dto.EventDTO;
import com.trytara.subwaytalent.web.rest.filter.EventFilter;
import com.trytara.subwaytalent.web.rest.util.PaginationUtil;

@RestController
@RequestMapping("/api")
public class EventResource {
	
	@Autowired
	EventRepository eventRepository;
	
	@Autowired
	UserResource userResource;

	@RequestMapping(value="/events", method=RequestMethod.POST)
	public void save(@RequestBody EventDTO request, @RequestHeader(value="X-session", required=false) String session){
		Event event = request.toEvent();
		if(event.id == null){
			event.dateCreated = new Date();
			event.status = EventStatus.CREATED.toString();
		}
		
		if(session != null){
			User user = userResource.getUser(session);
			if(user != null){
				event.createdBy = user;
			}
		}
		
		eventRepository.save(event);
	}
	

	@RequestMapping(value="/events", method=RequestMethod.GET)
	public ResponseEntity<List<EventDTO>> list(@RequestParam(value="page", defaultValue="1") int page, 
			@RequestParam(value="per_page", defaultValue="10") int size,
			@RequestParam(value="sort", defaultValue="id") String sort,
			@RequestParam(value="direction", defaultValue="DESC") Direction direction,
			@RequestParam(value="type", required=false) String type,
			@RequestParam(value="date", required=false) String date,
			@RequestParam(value="dateStartL", required=false) String dateStartL,
			@RequestParam(value="dateStartG", required=false) String dateStartG,
			@RequestParam(value="dateEndL", required=false) String dateEndL,
			@RequestParam(value="dateEndG", required=false) String dateEndG,
			@RequestParam(value="search", required=false) String search,
			HttpServletRequest request) throws URISyntaxException{
		Pageable pageRequest = PaginationUtil.generatePageRequest(page, size, new Sort(direction,sort));
		Page<Event> pageResult = eventRepository.findAll(new EventFilter(request), pageRequest);
		HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(pageResult, "/api/events", page, size);
		return new ResponseEntity<List<EventDTO>>(EventDTO.toDTOs(pageResult.getContent()), headers, HttpStatus.OK);
	}


	@RequestMapping(value="/events/mine", method=RequestMethod.GET)
	public ResponseEntity<List<EventDTO>> listMyEvents(@RequestParam(value="page", defaultValue="1") int page, 
			@RequestParam(value="per_page", defaultValue="10") int size,
			@RequestParam(value="sort", defaultValue="id") String sort,
			@RequestParam(value="direction", defaultValue="DESC") Direction direction,
			@RequestHeader(value="X-session", required=true) String session,
			HttpServletRequest request) throws URISyntaxException{
		Pageable pageRequest = PaginationUtil.generatePageRequest(page, size, new Sort(direction,sort));

		User user = userResource.getUser(session);
		Page<Event> pageResult = eventRepository.findByCreatedBy(user, pageRequest);
		HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(pageResult, "/api/events/me", page, size);
		return new ResponseEntity<List<EventDTO>>(EventDTO.toDTOs(pageResult.getContent()), headers, HttpStatus.OK);
	}

	@RequestMapping(value="/events/{id}", method=RequestMethod.GET)
	public ResponseEntity<EventDTO> get(@PathVariable("id") Long id) throws URISyntaxException{
		Event event = eventRepository.findOne(id);
		event.toString();
		return new ResponseEntity<EventDTO>(new EventDTO(event), HttpStatus.OK);
	}

	//TODO UPDATE EVENT STATUS
	

}
