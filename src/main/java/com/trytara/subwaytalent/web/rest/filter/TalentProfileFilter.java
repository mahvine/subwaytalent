package com.trytara.subwaytalent.web.rest.filter;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.trytara.subwaytalent.model.TalentProfile;
import com.trytara.subwaytalent.model.User;
import com.trytara.subwaytalent.model.UserProfile;
import com.trytara.subwaytalent.web.rest.util.SpecificationFilter;

public class TalentProfileFilter extends SpecificationFilter<TalentProfile> {

	private static final Logger logger = LoggerFactory.getLogger(TalentProfileFilter.class);
	

	public TalentProfileFilter(HttpServletRequest request) {
		super(request);
	}

	@Override
	public void createPredicates(Root<TalentProfile> root, CriteriaQuery<?> criteria,	CriteriaBuilder criteriaBuilder, List<Predicate> predicates) {

		/*String name = getParam("name");
		if(name != null){
			if(!name.isEmpty()){
				Predicate nameFilter = criteriaBuilder.like(root.<String>get("name"), "%"+name+"%");
				predicates.add(nameFilter);
			}
		}*/

		String search = getParam("search");
		if(search!=null){
			Predicate bioFilter = criteriaBuilder.like(root.<String>get("bio"), "%"+search+"%");
			Predicate locationFilter = criteriaBuilder.like(root.<String>get("location"), "%"+search+"%");
			Predicate nameFilter = criteriaBuilder.like(root.<User>get("user").<UserProfile>get("profile").<String>get("name"), "%"+search+"%");
			Predicate skillsFilter = criteriaBuilder.isMember(search,root.<Set<String>>get("skills"));
			Predicate genresFilter = criteriaBuilder.isMember(search,root.<Set<String>>get("genres"));
			List<Predicate> searchPredicates = new ArrayList<Predicate>();
			searchPredicates.add(bioFilter);
			searchPredicates.add(locationFilter);
			searchPredicates.add(nameFilter);
			searchPredicates.add(skillsFilter);
			searchPredicates.add(genresFilter);
			Predicate searchPredicate = criteriaBuilder.or(searchPredicates.toArray(new Predicate[searchPredicates.size()]));
			predicates.add(searchPredicate);
		}
		
		
	}
}
