/**
 * 
 */
package com.trytara.subwaytalent.web.rest.dto.account;

import java.util.Date;

import com.trytara.subwaytalent.model.User;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * @author JRDomingo
 * 
 */
@ApiModel
public class RegisterDTO extends LoginDTO{

	@ApiModelProperty(value = "Account type ", required = true)
	public User.Type type;

    public String name;
    
    public String pictureUrl;
    
    public Date birthDate;
	
}
