package com.trytara.subwaytalent.web.rest.filter;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang3.time.DateUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.trytara.subwaytalent.model.Event;
import com.trytara.subwaytalent.web.rest.util.SpecificationFilter;

public class EventFilter extends SpecificationFilter<Event> {

	private static final Logger logger = LoggerFactory.getLogger(EventFilter.class);
	

	public EventFilter(HttpServletRequest request) {
		super(request);
	}

	@Override
	public void createPredicates(Root<Event> root, CriteriaQuery<?> criteria,	CriteriaBuilder criteriaBuilder, List<Predicate> predicates) {

		String title = getParam("title");
		if(title != null){
			if(!title.isEmpty()){
				Predicate titleFilter = criteriaBuilder.like(root.<String>get("title"), "%"+title+"%");
				predicates.add(titleFilter);
			}
		}

		String type = getParam("type");
		if(type != null){
			if(!type.isEmpty()){
				Predicate typeFilter = criteriaBuilder.like(root.<String>get("type"), type);
				predicates.add(typeFilter);
			}
		}


		Date date = getDateParam("date");
		if(date != null){
			Predicate dateStartFilter = criteriaBuilder.lessThan(root.<Date>get("dateStart"), DateUtils.setHours(date, 23));
			predicates.add(dateStartFilter);
			Predicate dateEndFilter = criteriaBuilder.greaterThan(root.<Date>get("dateEnd"), DateUtils.setHours(date, 0));
			predicates.add(dateEndFilter);
		}


		Date dateStartLess = getDateParam("dateStartL");
		if(dateStartLess != null){
			Predicate dateStartFilter = criteriaBuilder.lessThan(root.<Date>get("dateStart"), dateStartLess);
			predicates.add(dateStartFilter);
		}

		Date dateStartGreater = getDateParam("dateStartG");
		if(dateStartGreater != null){
			Predicate dateFilter = criteriaBuilder.greaterThan(root.<Date>get("dateStart"), dateStartGreater);
			predicates.add(dateFilter);
		}

		Date dateEndLess = getDateParam("dateEndL");
		if(dateEndLess != null){
			Predicate dateFilter = criteriaBuilder.lessThan(root.<Date>get("dateEnd"), dateEndLess);
			predicates.add(dateFilter);
		}

		Date dateEndGreater = getDateParam("dateEndG");
		if(dateEndGreater != null){
			Predicate dateFilter = criteriaBuilder.greaterThan(root.<Date>get("dateEnd"), dateEndGreater);
			predicates.add(dateFilter);
		}

		String search = getParam("search");
		if(search!=null){
			Predicate nameFilter = criteriaBuilder.like(root.<String>get("title"), "%"+search+"%");
			Predicate descriptionFilter = criteriaBuilder.like(root.<String>get("description"), "%"+search+"%");
			Predicate typeFilter = criteriaBuilder.like(root.<String>get("type"), "%"+search+"%");
			Predicate categoryFilter = criteriaBuilder.like(root.<String>get("category"), "%"+search+"%");
			List<Predicate> searchPredicates = new ArrayList<Predicate>();
			searchPredicates.add(nameFilter);
			searchPredicates.add(descriptionFilter);
			searchPredicates.add(typeFilter);
			searchPredicates.add(categoryFilter);
			Predicate searchPredicate = criteriaBuilder.or(searchPredicates.toArray(new Predicate[searchPredicates.size()]));
			predicates.add(searchPredicate);
		}
		

		
	}
}
