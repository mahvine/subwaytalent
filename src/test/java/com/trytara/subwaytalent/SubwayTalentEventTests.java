package com.trytara.subwaytalent;

import static org.junit.Assert.assertEquals;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashSet;

import org.apache.commons.lang3.time.DateUtils;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.test.context.junit4.SpringRunner;

import com.trytara.subwaytalent.web.rest.dto.EventDTO;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
public class SubwayTalentEventTests {
	private static final Logger logger = LoggerFactory.getLogger(SubwayTalentEventTests.class); 

    @Autowired
    private TestRestTemplate restTemplate;
    

    
    @Test
    public void testCreatingEvent() {
    	EventDTO eventDTO = new EventDTO();
    	eventDTO.title = "Test1";
    	eventDTO.category = "Category1";
    	eventDTO.preferredGenres = new HashSet<String>();
    	eventDTO.preferredGenres.add("genre1");
    	eventDTO.preferredGenres.add("genre2");
    	eventDTO.preferredGenres.add("genre3");
    	logger.info("Creating event: {}",eventDTO);
    	this.restTemplate.postForObject("/api/events",  eventDTO, Void.class);
        EventDTO eventDTOResponse = this.restTemplate.getForObject("/api/events/1", EventDTO.class);
    	logger.info("Retrieved eventResponse: {}",eventDTOResponse);
    	assertEquals(eventDTO.preferredGenres, eventDTOResponse.preferredGenres);
    }


    
    @Test
    public void testDateFilteringOfEvent() {
    	EventDTO eventDTO = new EventDTO();
    	eventDTO.title = "Test2";
    	Date today = new Date();
    	eventDTO.dateStart = DateUtils.setHours(today, 0);
    	eventDTO.dateEnd = DateUtils.setHours(today, 23);
    	logger.info("Creating event: {}",eventDTO);
    	this.restTemplate.postForObject("/api/events",  eventDTO, Void.class);
    	Date tomorrow = DateUtils.addDays(today, 1);
    	DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd\'T\'HH:mm:ssZZZZZ");
    	

    	String tomorrowString = dateFormat.format(tomorrow);
    	EventDTO[] eventDTOsResponse = this.restTemplate.getForObject("/api/events?date="+tomorrowString, EventDTO[].class);
    	logger.info("Retrieved tomorrow filterString:{} eventResponse: {}",tomorrowString,eventDTOsResponse);
    	assertEquals(0, eventDTOsResponse.length);

    	String todayString = dateFormat.format(today);
    	eventDTOsResponse = this.restTemplate.getForObject("/api/events?date="+todayString, EventDTO[].class);
    	logger.info("Retrieved today filterString:{} eventResponse: {}",todayString, eventDTOsResponse);
    	assertEquals(1, eventDTOsResponse.length);
    	

    	eventDTOsResponse = this.restTemplate.getForObject("/api/events?dateStartL="+todayString, EventDTO[].class);
    	logger.info("Retrieved dateStartL filterString:{} eventResponse: {}",todayString, eventDTOsResponse);
    	assertEquals(1, eventDTOsResponse.length);

    	eventDTOsResponse = this.restTemplate.getForObject("/api/events?dateStartG="+todayString, EventDTO[].class);
    	logger.info("Retrieved today filterString:{} eventResponse: {}",todayString, eventDTOsResponse);
    	assertEquals(0, eventDTOsResponse.length);
    	
    	
    }

	
}
